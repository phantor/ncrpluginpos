# coding: utf-8
import requests
from requests import Request
import uuid
import json
from config import ENDPOINT_AUTH, ENDPOINT_GEN_PAY_REQ, DEBUG_REST, PLUGIN_NAME, ENDPOINT_FIND
import util
from colibri_mod import obter_maquina_atual

ERROR_NOT_AUTH = -1
AUTH_OK = 200

#cria o objeto de sessão web
sessao = requests.Session()

#autentica no backend Vee
def autenticate( user, password):
    head = {"Accept": "application/json",
            "Content-type": "application/x-authc-username-password+json",
            "Accept-Language": "en"}

    parameters = "{\"userId\": \"%s\", \"password\": \"%s\"}" % (user, password)

    req = Request("POST", ENDPOINT_AUTH, headers=head, data=parameters)
    prepared = sessao.prepare_request(req)
    if(DEBUG_REST):
        debug_POST(prepared)

    ret = sessao.send(prepared)

    if(ret.ok):
        if(DEBUG_REST):
            jData = json.loads(ret.content)
            print("The response contains {0} properties".format(len(jData)))
            print("\n")
            for key in jData:
                print key + " : " + jData[key]
            else:
                ret.raise_for_status
        return AUTH_OK
    else:
        ret.raise_for_status
        return ERROR_NOT_AUTH


def paymentRequest( valor, token, cpf ):

    maquina = obter_maquina_atual(PLUGIN_NAME)
    configs = util.obter_configuracoes( maquina )

    ret = autenticate( configs["user"], configs["passwd"])

    if(ret != AUTH_OK):
        return "Erro de autenticação no backend Vee. Confira as configurações do plugin."

    #gera um uuid unico para a transação e devolve com resposta caso a requisiçãoseja valida
    transaction_uuid =  uuid.uuid4()

    head = {"Accept": "application/json",
            "Content-type": "application/json",
            "Accept-Language": "en"}

    dados_body = "{\"amount\": %s, \"paymentToken\": \"%s\", \"uuid\":\"%s\", \"document\":\"%s\"}" % (valor, token, transaction_uuid, cpf)

    req = Request("POST", ENDPOINT_GEN_PAY_REQ, headers=head, data=dados_body )
    prepared = sessao.prepare_request( req )
    #debug_POST( prepared )
    ret = sessao.send( prepared )

    if(ret.ok):
        jData = json.loads(ret.content)

        #for key in jData:
        #    print key + " : " 
        #    print( jData[key] )

        if(jData["success"] ):
            return str(transaction_uuid)
        else:
        	   return ""
    else:
        ret.raise_for_status
        retorno = ret.content
        return ""



def find( transaction_id ):

    maquina = obter_maquina_atual(PLUGIN_NAME)
    configs = util.obter_configuracoes( maquina )

    head = {"Accept": "application/json",
            "Content-type": "application/json",
            "Accept-Language": "en"}

    dados_body = "{\"transactionUUID\": \"%s\"}" % (transaction_id)

    req = Request("POST", ENDPOINT_FIND, headers=head, data=dados_body )
    prepared = sessao.prepare_request( req )
    #debug_POST( prepared )
    ret = sessao.send( prepared )

    #print(dados_body)
    #print(ret.ok, ret.content )

    if(ret.ok):
        jData = json.loads(ret.content)
        if( 'rejectReason' in jData ):
            return jData

    else:
        ret.raise_for_status
        retorno = ret.content
        return json.loads(u'{"erro":"Transação não encontrada"}')



#serve para deputar uma requisição preparada e verificar o que será enviado ao servidor
def debug_POST(req):
    print('{}\n{}\n{}\n\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body
    ))
    print('-----------END----------')