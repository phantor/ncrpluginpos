# coding: utf-8

# Nome e versão do Plugin, obrigatórios
PLUGIN_NAME = 'Vee Pay NCR-POS'
MEIO_VEE = 100
ENDPOINT_AUTH = "https://prod.veedigital.com.br/FinancialBackend/rest/authenticate"
ENDPOINT_GEN_PAY_REQ = "https://prod.veedigital.com.br/FinancialBackend/rest/private/payment/generatePaymentRequest"
ENDPOINT_FIND = "https://prod.veedigital.com.br/FinancialBackend/rest/private/transaction/find"
DEBUG_REST = False