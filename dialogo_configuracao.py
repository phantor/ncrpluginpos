# coding: utf-8
import win32con
import win32api
import win32ui
from pywin.mfc import dialog
import rest
from colibri_mod import gravar_config, obter_configs, obter_maquina_atual
import util

ID_BTNOK = 3
ID_BTNCANCELAR = 4
ID_USER = 5
ID_PASSWD = 6
ID_BTNTESTAR = 10

PLUGIN_NAME = 'Vee Pay NCR-POS'


class DialogoConfiguracao(dialog.Dialog):
    TITULO = u"Configuração do serviço Vee Pay NCR POS"
    configs = {}
    maquina = 0;

    @classmethod
    def template_dlg_cfg(cls):
        style = win32con.DS_MODALFRAME | win32con.WS_POPUP | \
            win32con.WS_VISIBLE | win32con.WS_CAPTION | \
            win32con.WS_SYSMENU | win32con.DS_SETFONT

        visible = win32con.WS_CHILD | win32con.WS_VISIBLE
        tcs = visible | win32con.WS_TABSTOP
        static = visible | win32con.SS_LEFT
        cb = tcs | win32con.WS_GROUP | win32con.WS_VSCROLL | \
            win32con.CBS_DROPDOWNLIST | win32con.WS_BORDER
        edt = tcs | win32con.ES_AUTOHSCROLL | win32con.ES_LEFT | \
            win32con.WS_BORDER
        btn = tcs | win32con.BS_VCENTER | win32con.BS_CENTER

        # Window frame and title
        dlg = [
            [cls.TITULO, (0, 0, 187, 118), style, None, (10, "Segoe UI")],
            ["STATIC", u"Informe as credenciais de acesso ao Backend Vee:", -1,
                (15, 10, 200, 7), static],
            ["STATIC", u"Usuário:", -1, (15, 35, 30, 8), static],
            ["STATIC", u"Senha:", -1, (15, 60, 21, 8), static],

            ["EDIT", u"", ID_USER, (15, 45, 154, 14), edt],
            ["EDIT", u"", ID_PASSWD, (15, 70, 154, 14), edt],

            ["BUTTON", u"&Ok", ID_BTNOK, (118, 92, 50, 14), btn | win32con.BS_DEFPUSHBUTTON],
            ["BUTTON", u"&Cancelar", ID_BTNCANCELAR, (65, 92, 50, 14), btn],
            ["BUTTON", u"&Testar", ID_BTNTESTAR, (15, 92, 48, 14), btn]
        ]

        return dlg

    def __init__(self):
        dialog.Dialog.__init__(self, self.template_dlg_cfg())
        self.maquina = obter_maquina_atual(PLUGIN_NAME)
        #win32api.MessageBox(0, str(maquina), u'Maquina')
        self.configs = util.obter_configuracoes( self.maquina )
        #win32api.MessageBox(0, str( self.configs ), u'Maquina')
        self.HookCommand(self.click_aplicar, ID_BTNOK)
        self.HookCommand(self.click_fechar, ID_BTNCANCELAR)

    def OnInitDialog(self):
        rc = dialog.Dialog.OnInitDialog(self)
        #self.butOK = self.GetDlgItem(ID_BTNOK)
        #self.butCancel = self.GetDlgItem(ID_BTNCANCELAR)
        self.edtUSER = self.GetDlgItem(ID_USER)
        self.edtPASSWD = self.GetDlgItem(ID_PASSWD)
        self.edtUSER.SetWindowText(self.configs.get('user'))
        self.edtPASSWD.SetWindowText(self.configs.get('passwd', ''))
        self.HookCommand(self.testar, ID_BTNTESTAR)
        return rc


    def gravar(self):
        configs = dict(
            user=self.edtUSER.GetWindowText(),
            passwd=self.edtPASSWD.GetWindowText()
        )
        util.gravar_configuracoes(self.maquina, **configs)

    def testar(self, id, code):
        self.gravar()
        resultado = rest.autenticate(rest.sess, self.edtUSER.GetWindowText(), self.edtPASSWD.GetWindowText()  )
        if( resultado == 200):
            win32api.MessageBox(0, u'Teste de autenticação realizado com sucesso', u'Teste de Autenticação')
        else:
            win32api.MessageBox(0, u'Falha no teste de autenticação. Reveja as credenciais de acesso', u'Teste de Autenticação')

    def click_aplicar(self, id, code):
        self.gravar()
        self.EndDialog(win32con.IDOK)

    def click_fechar(self, id, code):
        self.EndDialog(win32con.IDOK)

    


def executar():
    d = DialogoConfiguracao( )
    d.DoModal()


#executar()