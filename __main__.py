# coding: utf-8
import json
import os
from util import obter_caminho_plugin, criar_logger, codifica_retorno, retornar, obter_configuracoes
from colibri_mod import callback, assinar_evento, obter_configs, gravar_config, mostrar_mensagem, mostrar_teclado
import dialogo_configuracao
import dialogo_genpayreq


PLUGIN_NAME = 'Vee Bussines NCR-POS'
PLUGIN_VERSION = '1.0.0.0'
MEIO_VEE = 100

DADOS_FABRICANTE = {
    "fabricante":
        {
            "empresa": "Vee Digital Tecnologia S.A.",
            "desenvolvedor": "Dpto. de Integracao e Automação",
            "termos_da_licenca": "Creative Commons for Business",
            "direitos_de_copia": "Vee Digital Tecnologia S.A.",
            "marcas_registradas": "Vee, Vee Digital, Vee Business, Vee Pay, WebVee, Digital Value, DGV",
        },
    "suporte":
        {
            "email": "daniel.basconcello@vee.digital",
            "url": "https://vee.digital",
            "telefone": "(11) 95901-0160"
        },
}


# Logger para testes
log_file_name = os.path.join(obter_caminho_plugin(), PLUGIN_NAME + '.log')
logger = criar_logger(__name__, log_file_name)

# Funções obrigatórias
def obter_dados_fabricante():
    return retornar( **DADOS_FABRICANTE )

# Funções opcionais: Implemente as que desejar
def ativar(maquina):
    logger.debug('plugin ativado na maquina: %d', maquina)


def desativar(maquina):
    logger.debug('plugin desativado na maquina: %d', maquina)

    
def configurar(maquinas):
    logger.debug('configurar: %s', maquinas)
    #jData = json.loads( maquinas )
    dialogo_configuracao.executar()

    
def notificar(evento, contexto):
    # logger.debug('notificar: evento %s, contexto %s' %        (evento, contexto)    )
    try:
        logger.debug("Abrindo o vee pay NCR pelo evento <%s> no contexto <%s>" % (evento, contexto))
        

        jData = json.loads( contexto )

        #verifica se o meio de pagamento e vee    
        meio = jData["item_de_caixa"]["meio"]["codigo"]
        if( meio == MEIO_VEE ):
          valor_pagamento = jData["item_de_caixa"]["valor"]
          #mostrar_mensagem(PLUGIN_NAME,retornar(mensagem="teste", tipo="info", titulo=valor_pagamento) )
          retorno_teclado =  json.loads( mostrar_teclado(PLUGIN_NAME, retornar(titulo="Informe o Token de pagamento ou o CPF do cliente", tipo_teclado="numerico", maximo=11 )))

          #se o teclado retornou preenchido
          if( retorno_teclado["retorno"] ):
            token = retorno_teclado["resposta"]
            retorno_dialogo = dialogo_genpayreq.executar(valor_pagamento, token)
            #mostrar_mensagem(PLUGIN_NAME,retornar(mensagem=str(retorno_dialogo) , tipo="info", titulo="retorno") )
            if( str(retorno_dialogo) == "1" ):
              pass
            else:
              return retornar(erro=str("Pagamento com Vee Cancelado"))
          else:
            return retornar(erro=str("Pagamento com Vee Cancelado"))
            #mostrar_mensagem(PLUGIN_NAME,retornar(mensagem="Pagamento com Vee Cancelado", tipo="info", titulo="Pagar com Vee") )
        pass  # Tratar a notificação
    except Exception as erro:
        logger.error("falha notificacao de %s: %s" % (evento, erro)) 
        return retornar(erro=str(erro))
    return ''


def verificar_versao(informacao):
    info = json.loads(informacao)
    if info['versao'] >= 2000:
        return retornar(erro='Não suporta versão >= 2000')
    return ''  # Nenhuma objeção a essa versão


def registrar_assinaturas():
    logger.debug('registrar_assinaturas')
    eventos = ['EventoDeCaixa.AoAdicionarItemDeCaixa']  # Lista de eventos assinados pelo plugin
    for evt in eventos:
        logger.debug('assinar_evento: %s, %s', PLUGIN_NAME, evt)
        assinar_evento(PLUGIN_NAME, evt)


def obter_macro(uma_macro):
    exemplo_macros = {'nome_macro': 'valor_macro'}
    logger.debug('obter_macro: %s(%s)', uma_macro, exemplo_macros.get(uma_macro))
    try:
        return retornar(valor=exemplo_macros[uma_macro])
    except KeyError:
        return retornar(erro='Macro desconhecida')

#dialogo_genpayreq.executar(3, "123452")
#obter_configuracoes(0)
#dialogo_configuracao.executar(3)