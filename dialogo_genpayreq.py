# coding: utf-8
import win32con
from pywin.mfc import dialog
import cpf
import rest
import win32api
import util
import time
from config import PLUGIN_NAME
from colibri_mod import  mostrar_mensagem

#from stoppableThread import StoppableThread 

from threading import Thread


ID_BTNCANCELAR = 4
ID_LABEL = 1

class DialogoPagamento(dialog.Dialog):

    TITULO = u"Pagamento com Vee"

    configs = {}
    maquina = 0;
    valor = 0
    identificador = ""

    thread_find = Thread()
    sai_thread = False
    cancelado = False
    sucesso = False
    encerrado = False

    @classmethod
    def template_dlg_cfg(cls):
        style = win32con.DS_MODALFRAME | win32con.WS_POPUP | \
            win32con.WS_VISIBLE | win32con.WS_CAPTION | \
            win32con.WS_SYSMENU | win32con.DS_SETFONT

        visible = win32con.WS_CHILD | win32con.WS_VISIBLE
        tcs = visible | win32con.WS_TABSTOP
        static = visible | win32con.SS_LEFT
        cb = tcs | win32con.WS_GROUP | win32con.WS_VSCROLL | \
            win32con.CBS_DROPDOWNLIST | win32con.WS_BORDER
        edt = tcs | win32con.ES_AUTOHSCROLL | win32con.ES_LEFT | \
            win32con.WS_BORDER
        btn = tcs | win32con.BS_VCENTER | win32con.BS_CENTER

        valor_pagamento = visible | win32con.SS_RIGHT

        # Window frame and title
        dlg = [
            [cls.TITULO, (0, 0, 210, 100), style, None, (14, "Segoe UI")],
            ["STATIC", u"Enviando solicitação de pagamento para o backend", ID_LABEL, (15, 15, 180, 50), static],
            ["BUTTON", u"&Cancelar", ID_BTNCANCELAR, (50, 76, 100, 14), btn],
        ]

        return dlg

    def __init__(self, valor, identificador):
        dialog.Dialog.__init__(self, self.template_dlg_cfg())
        self.HookCommand(self.click_fechar, ID_BTNCANCELAR)
        self.valor = valor
        self.identificador = identificador

    def OnInitDialog(self):
        rc = dialog.Dialog.OnInitDialog(self)
        self.butCancel = self.GetDlgItem(ID_BTNCANCELAR)
        self.lblWarning = self.GetDlgItem( ID_LABEL )
        
        self.thread_find = Thread(target=self.find)
        self.thread_find.start()
        return rc


    def teste( self ):
        count = 0
        while ( count < 30 and not self.sai_thread ):   
            print( count) 
            count = count + 1 
            self.lblWarning.SetWindowText( str(count) )
            time.sleep( 0.2 )

       

    def find(self):

        mensagem = ""
        transaction_uuid = ""

        # se foi usado CPF, passa o CPF
        if(len(self.identificador) > 0 and len(self.identificador) == 6):
            transaction_uuid = rest.paymentRequest( self.valor, self.identificador, "")
        else:
            if( len(self.identificador) > 0 and (len(self.identificador) == 11 or len(self.identificador) == 14)):
                # valida o CPF
                valida = cpf.CPF( self.identificador )
                print( valida.isValid() )
                if( valida.isValid() ):
                    transaction_uuid = rest.paymentRequest( self.valor, "", self.identificador)
                else:
                    mensagem = u"CPF Inválido! \n\nConfira o número com o consumidor"
            else:
                mensagem = u"Identificador inválido.\n\nInsira um CPF válido ou um Token"

        #se ocorreu transação iniciada com sucesso, inicia o looping da find
        if( len(transaction_uuid) > 0 ):
            #tempo em segundos para checagem da transação
            qtd_seg_checagem = 120

            #segundos entre cada chamada no serviço Find    
            find_div = 3

            #intervalo de tempo do looping    
            dt = 0.5    

            #quantidade de loopings = qtd_seg_checagem * dt
            ql = qtd_seg_checagem * dt

            #quantidade de loopings para cada find
            ql_mod_find = find_div * dt    

            count_checa = 0;

            self.sucesso = False;

            while (count_checa < ql and not self.sai_thread):        

                self.lblWarning.SetWindowText( "Aguardando pagamento pelo consumidor: \n\n" + str( count_checa )   )

                if(count_checa % ql_mod_find == 0 ):

                    retorno = rest.find( transaction_uuid )
                    #retorno = util.retornar(erro="transacao nao encontrada")

                    if( "erro" not in retorno):
                        
                        if( retorno["rejectReason"] is None ):
                            self.sucesso = True
                            mensagem = u"Transação concluída com sucesso!\n\n" + u"Valor da venda: R$ " + str(retorno["amount"]) + u"\n\nCódigo único de transação: " + str(retorno["transactionId"]) + u"\n\nData da transação: " + str(retorno["dtConfirmed"])
                        else:
                            mensagem = retorno["rejectReason"]
                        self.sai_thread = True

                count_checa = count_checa + 1
                time.sleep( dt )

            if( not self.sai_thread ):
                mensagem = u"A transação expirou!"
        else:
            mensagem = u"Token Não encontrado!"

        if( self.cancelado ):
            mensagem = u"Pagamento cancelado!"
            self.sucesso = False;

        self.lblWarning.SetWindowText( mensagem  )
        #mostrar_mensagem(PLUGIN_NAME, util.retornar(mensagem=mensagem, tipo="aviso", titulo=u'Aviso!') )
        
        self.encerrado = True
        self.butCancel.SetWindowText( "Fechar" )
        
        return

    def click_fechar(self, id, code):
        if( not self.encerrado ):
            self.sai_thread = True
            self.cancelado = True
        else:
            if( self.sucesso ):
                self.EndDialog(win32con.IDOK) #returna 1
            else:
                self.EndDialog(win32con.IDCANCEL) #retorna 2
       
  
def executar(valor, identificador):
    d = DialogoPagamento(  valor, identificador)
    return d.DoModal()

#executar(0.01, "275.990.108-40")
