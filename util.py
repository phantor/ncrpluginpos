# coding: utf-8

import logging
import os
import json
from colibri_mod import obter_configs, gravar_config
from config import PLUGIN_NAME

# obtem caminho do plugin
def obter_caminho_plugin():
    cam = os.path.dirname(__file__)
    while not os.path.isdir(cam):
        novo = os.path.dirname(cam)
        if novo == cam:
            return
        cam = novo
    return cam

# cria um logger para seu plugin
def criar_logger(logger_name, log_file, level=logging.DEBUG):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w+')
    fileHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    return l


# codifica um string de retorno para cp1252, encoding conhecido pelo Colibri
def codifica_retorno(texto):
    if isinstance(texto, unicode):
        return texto.encode('cp1252')
    return texto.decode('utf-8').encode('cp1252')


def retornar(**kwargs):
    return codifica_retorno(json.dumps(kwargs))


def gravar_configuracoes( maquina, **kwargs):
    for item in kwargs.items():
        gravar_config(PLUGIN_NAME, item[0], maquina, item[1])


def obter_configuracoes(maquina, falha_sem_configs=False):

    cfg = json.loads(obter_configs(PLUGIN_NAME, maquina))['configs']

    if len(cfg) == 0 and falha_sem_configs:
        raise Exception("Plugin Vee nao configurado")

    cfg_persistidas = {}
    for key, value in cfg.iteritems():
        cfg_persistidas[key] = value['valor']

    cfg_padroes = {
        "user": 'phantor',
        "passwd": '1579081'
    }

    cfg_padroes.update(cfg_persistidas)
    return cfg_padroes        